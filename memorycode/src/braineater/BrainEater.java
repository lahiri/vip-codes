/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package braineater;

/**
 *
 * @author Aditya
 */
public class BrainEater { //Class which eats the heap 

    
 void recur() // a bad recursion function
 {
 
   while(true)//While loop with an extremely bad termination point
    {
     String s1 = new String("The lord of Castemere marched on Winterfell");//allocating new memory
     recur();//deadly function call
    } 
  }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BrainEater B1 = new BrainEater();
        try{
           B1.recur();//Calls the Function belonging to class BrainEater
           }
        catch(StackOverflowError E)// Writes the statement in case of a StackOverflow
              {
               System.out.println("You have successfully created a stack overflow\nTherefore, you have run out of memory.");
              }
           }
  }

